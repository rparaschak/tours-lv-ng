export interface Tour {
  id: string;
  name: string;
  description: string;
  coverImageUrl?: string;
  points?: any[];
  images?: any[];

  meta?: {
    distance?: number;
    people?: number;
    time?: number;
    points?: number;
  };
}
