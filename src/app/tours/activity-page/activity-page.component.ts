import { Component, OnInit } from '@angular/core';
import { ActivityService } from '../../activity/activity.service';

@Component({
  selector: 'app-activity-page',
  templateUrl: './activity-page.component.html',
  styleUrls: ['./activity-page.component.css']
})
export class ActivityPageComponent implements OnInit{

  public activity: any;

  constructor(private activityService: ActivityService) {
  }

  ngOnInit() {
    this.getActivity();
  }

  getActivity() {
    return this.activityService.getActivity()
      .subscribe(activity => {
        this.activity = activity;
      });
  }

}
