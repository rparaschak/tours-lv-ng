import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tours-list',
  templateUrl: './tours-list.component.html',
  styleUrls: ['./tours-list.component.scss']
})
export class ToursListComponent implements OnInit {

  @Input('tours') tours: any[];

  constructor() { }

  ngOnInit() {
  }

}
