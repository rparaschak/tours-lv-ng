import {Component, OnInit, Input} from '@angular/core';
import {Tour} from '../../tour.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tours-list-item',
  templateUrl: './tours-list-item.component.html',
  styleUrls: ['./tours-list-item.component.scss']
})
export class ToursListItemComponent implements OnInit {

  @Input('tour') tour: Tour;

  constructor(private router: Router) {
  }

  ngOnInit() {}

  joinButtonClick(tour: Tour) {
    this.router.navigate(['/tour', tour.id]);
  }

}
