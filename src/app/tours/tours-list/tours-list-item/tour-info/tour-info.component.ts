import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tour-info',
  templateUrl: './tour-info.component.html',
  styleUrls: ['./tour-info.component.scss']
})
export class TourInfoComponent implements OnInit{

  @Input() tourMeta: any;

  constructor() {
  }

  ngOnInit() {
  }

}
