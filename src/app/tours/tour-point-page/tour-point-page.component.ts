import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToursService } from '../tours.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-tour-point-page',
  templateUrl: './tour-point-page.component.html',
  styleUrls: ['./tour-point-page.component.css']
})
export class TourPointPageComponent implements OnInit {

  public point: any;

  constructor(private route: ActivatedRoute, private router: Router, private toursService: ToursService) {}

  ngOnInit() {
    this.route.params
      .map(params => {
        return this.getTourPoint({tourId: params.tourId, pointId: params.pointId});
      })
      .subscribe();
  }

  getTourPoint(params) {
    return this.toursService.getTourPoint({
      tourId: params.tourId,
      pointId: params.pointId
    }).subscribe(point => {
      this.point = point;
    });
  }

  backToTour() {
    this.route.params
      .map(params => {
        this.router.navigate(['/tour', params.tourId]);
      })
      .subscribe();
  }

}
