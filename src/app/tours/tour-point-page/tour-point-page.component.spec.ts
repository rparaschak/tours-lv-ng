import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourPointPageComponent } from './tour-point-page.component';

describe('TourPointPageComponent', () => {
  let component: TourPointPageComponent;
  let fixture: ComponentFixture<TourPointPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourPointPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourPointPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
