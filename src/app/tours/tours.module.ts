import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UICarouselModule } from 'ui-carousel';

import { ToursService } from './tours.service';

import { ToursPageComponent } from './tours-page/tours-page.component';
import { TourPageComponent } from './tour-page/tour-page.component';
import { TourPointsListComponent } from './tour-page/tour-points-list/tour-points-list.component';
import { TourPointPageComponent } from './tour-point-page/tour-point-page.component';

import { ToursListComponent } from './tours-list/tours-list.component';
import { ToursListItemComponent } from './tours-list/tours-list-item/tours-list-item.component';
import { TourInfoComponent } from './tours-list/tours-list-item/tour-info/tour-info.component';
import { CoreModule } from '../core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { ActivityModule } from '../activity/activity.module';
import { ActivityPageComponent } from './activity-page/activity-page.component';


@NgModule({
  imports: [
    CommonModule,
    UICarouselModule,
    HttpClientModule,
    ActivityModule,
    CoreModule
  ],
  declarations: [
    ToursPageComponent, TourPageComponent, TourPointsListComponent, TourPointPageComponent, ToursListComponent,
    ToursListItemComponent, TourInfoComponent, ActivityPageComponent],
  providers: [
    ToursService
  ],
  exports: [ TourPageComponent ]
})
export class ToursModule { }
