import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tour } from '../../tour.interface';

@Component({
  selector: 'app-tour-points-list',
  templateUrl: './tour-points-list.component.html',
  styleUrls: ['./tour-points-list.component.scss']
})
export class TourPointsListComponent implements OnInit, OnChanges{

  @Input() tourPoints: any[];
  @Input() tour: Tour;
  @Input() checkins: any[];

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {

  }

  ngOnChanges(changes) {
    if (changes.checkins && changes.checkins.currentValue) {
      this.tourPoints.map((point) => {
        point.checkin = this.checkins.find(checkin =>  checkin.pointId === point.id );
        return point;
      });
    }
  }

  goToPoint(point) {
    this.router.navigate(['/tour', this.tour.id, 'point', point.id]);
  }
}
