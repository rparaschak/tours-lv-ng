import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourPointsListComponent } from './tour-points-list.component';

describe('TourPointsListComponent', () => {
  let component: TourPointsListComponent;
  let fixture: ComponentFixture<TourPointsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourPointsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourPointsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
