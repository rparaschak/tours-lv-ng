import { Component, OnInit, OnDestroy, Input, OnChanges } from '@angular/core';
import { ToursService } from '../tours.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Tour } from '../tour.interface';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import { ActivityService } from '../../activity/activity.service';

@Component({
  selector: 'app-tour-page',
  templateUrl: './tour-page.component.html',
  styleUrls: ['./tour-page.component.scss']
})
export class TourPageComponent implements OnInit, OnChanges, OnDestroy{

  public tour: Tour;
  @Input() activity?: any;
  public tourPoints: any;

  /**
   * Subscriptions
   */
  private getActivitySubscription;
  private routeParamsSubscription;

  constructor(private toursService: ToursService, private activityService: ActivityService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {

    let routerParams;

    this.routeParamsSubscription = this.route.params
      .mergeMap(params => {
        routerParams = params;
        if (!routerParams && !this.activity) {
          throw new Error('if (!routerParams && !this.activity)');
        }
        return this.getTour(routerParams.tourId || this.activity.tourId);
      })
      .mergeMap(() => {
        return this.getActivity();
      })
      .mergeMap(activity => {
        if (activity && activity.tourId === this.tour.id) {
          this.activity = activity;
        }
        return this.getTourPoints();
      })
      .subscribe(() => {
      });
  }

  getTour(tourId) {
    return this.toursService.getTour(tourId)
      .map((tour) => {
        this.tour = tour;
        return tour;
      });
  }

  getTourPoints() {
    return this.toursService.getTourPoints(this.tour.id)
      .map(points => {
        this.tourPoints = points;
        return points;
      });
  }

  getActivity(): Observable<any> {
    if (this.activity) {
      return Observable.of(this.activity);
    } else {
      return this.activityService.getTourActivityStatus();
    }
  }

  startTour() {
    this.activityService.startTour(this.tour)
      .subscribe(() => {
        this.router.navigate(['/activity']);
      });
  }

  activityCheckin() {
    this.activityService.activityCheckin()
      .subscribe();
  }

  ngOnChanges() {
  }

  ngOnDestroy() {
    }

}
