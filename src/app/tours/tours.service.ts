import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/find';
import { catchError } from 'rxjs/operators';
import { Tour } from './tour.interface';
import { ToursHttpClient } from '../core/tours-http-client';

@Injectable()
export class ToursService {

  /** This is cache for tour objects */
  tours: Tour[];

  constructor(private http: ToursHttpClient) {
    this.tours = [];
  }

  /**
   * Get all nearby tours
   */
  getNearbyTours(): Observable<Tour[]> {
    return this.http.get<Tour[]>('/tour')
      .pipe(catchError(this.handleError('getNearbyTours', [])));
  }

  /**
   * Gets tour from cache or api endpoint
   * @param tourId
   * @returns {Observable<Tour>}
   */
  getTour(tourId): Observable<Tour> {

    if (this.tours.length) {
      return this.getCachedTour(tourId);
    }

    if (!this.tours.length) {
      return this.fetchTour(tourId);
    }


    return Observable.create(observer => {

      if (this.tours.length) {
        const tour = this.tours
          .find(t => t.id === tourId);
        observer.next(tour);
        return observer.complete();
      } else {
        return Observable.of({
          id: 'some-tour-1',
          name: 'tour#1',
          description: 'yolo',
          coverImageUrl: '/assets/images/temp/tour-grid-cover.jpg'
        });
      }

    });
  }

  getTourPoints(tourId): Observable<any> {
    return this.http.get<Tour>(`/tour/${tourId}/points`)
      .pipe(catchError(this.handleError('getTourPoints', {})));
  }

  getTourPoint(params) {
    return this.http.get<Tour>(`/tour/${params.tourId}/point/${params.pointId}`)
      .pipe(catchError(this.handleError('getTourPoint', {})));
  }

  private getCachedTour(tourId): Observable<Tour> {
    return Observable.create(observer => {
      const tour = this.tours
        .find(t => t.id === tourId);
      observer.next(tour);
      return observer.complete();
    });
  }

  private fetchTour(tourId): Observable<Tour> {
    return this.http.get<Tour>(`/tour/${tourId}`)
      .pipe(catchError(this.handleError('fetchTour', {} as Tour)));
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return Observable.of(result as T);
    };
  }

}
