import { Component, OnInit } from '@angular/core';
import { ToursService } from '../tours.service';
import { List } from 'immutable';

@Component({
  selector: 'app-tours-page',
  templateUrl: './tours-page.component.html',
  styleUrls: ['./tours-page.component.scss']
})
export class ToursPageComponent implements OnInit {

  public availableTours: List<any>;

  constructor(private toursService: ToursService) {
    toursService.getNearbyTours()
      .subscribe(
        (tours) => {
          this.availableTours = List(tours);
        },
        (err) => {
          alert(err);
        }
        );
  }

  ngOnInit() {
  }

}
