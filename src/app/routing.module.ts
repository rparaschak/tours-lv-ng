/**
 * Created by rparaschak on 2/8/17.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ToursPageComponent } from './tours/tours-page/tours-page.component';
import { TourPageComponent } from './tours/tour-page/tour-page.component';
import { ActivityPageComponent } from './tours/activity-page/activity-page.component';
import { TourPointPageComponent } from './tours/tour-point-page/tour-point-page.component';
import { SigninComponent } from './auth/signin/signin.component';


const routes: Routes = [
  {
    path: 'available-tours', component: ToursPageComponent
  },
  {
    path: 'tour/:tourId', component: TourPageComponent
  },
  {
    path: 'activity', component: ActivityPageComponent,
  },
  {
    path: 'tour/:tourId/point/:pointId', component: TourPointPageComponent,
  },
  {
    path: 'signin', component: SigninComponent
  },
  {
    path: '', redirectTo: '/available-tours', pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {
}
