import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
  }

  async facebookSignin() {
    try {
      let authStatus = await this.authService.getFacebookLoginStatus() as any;

      if (!authStatus.authorized) {
        authStatus = await this.authService.facebookLogin();
      }

      if ( !authStatus.authorized) {
        throw new Error('Failed to log in.');
      }

      const authResponse = await this.authService.facebookSignin(authStatus.authResponse);

    } catch (error) {
      console.log(error);
    }
  }

}
