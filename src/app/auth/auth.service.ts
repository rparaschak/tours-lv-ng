import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { ToursHttpClient } from '../core/tours-http-client';

declare const FB;

@Injectable()
export class AuthService {

  private facebookScope: String;
  private facebookAuthStatus: any;

  constructor( private http: ToursHttpClient) {
    this.facebookScope = 'public_profile,email';
  }


  getFacebookLoginStatus() {
    return new Promise((resolve, reject) => {
      FB.getLoginStatus(response => {
        if (response.status === 'not_authorized') {
          resolve({authorized: false});
        } else {
          resolve({authorized: true, authResponse: response.authResponse});
          this.facebookAuthStatus = response.authResponse;
        }
      });
    });
  }

  /** Gets facebook access token and authorization data */
  facebookLogin() {
    return new Promise((resolve, reject) => {
      FB.login(response => {
        console.log(response);
        if (response.status === 'connected') {
          resolve(response.authResponse);
          this.facebookAuthStatus = response.authResponse;
        } else {
          reject({status: 'login_fail'});
        }
      }, {scope: this.facebookScope});
    });
  }

  /** Not implemented*/
  facebookApi(endpoint, method = 'GET') {
    return new Promise((resolve, reject) => {
      return reject('Not impemented');
      /*FB.api(
        `/${this.facebookAuthStatus.userID}/${endpoint}`,
        method,
        response => {
          resolve(response);
        }
      );*/
    });
  }

  /** Actual signin and signup on the API server*/
  async facebookSignin({ accessToken, signedRequest, userID }) {
    const response = this.http
      .post<any>('/login/facebook', { accessToken, signedRequest, userID })
      .pipe(catchError(this.handleError('facebookSignin', [])));

    return response.toPromise();
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return Observable.of(result as T);
    };
  }

}
