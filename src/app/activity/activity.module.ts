import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityService } from './activity.service';
import { LeftNavActivityWidgetComponent } from './left-nav-activity-widget/left-nav-activity-widget.component';
import { HttpClientModule } from '@angular/common/http';
import { LocationService } from './location.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    ActivityService,
    LocationService
  ],
  declarations: [ LeftNavActivityWidgetComponent],
  exports: [ LeftNavActivityWidgetComponent ]
})
export class ActivityModule { }
