import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LocationService {

  constructor() {

  }

  getLocation() {
    return Observable.create( observer => {
      navigator.geolocation.getCurrentPosition( position => {
        observer.next({ lat: position.coords.latitude, lon: position.coords.longitude });
        return observer.complete();
      });
    });
  }

}
