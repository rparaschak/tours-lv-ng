import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftNavActivityWidgetComponent } from './left-nav-activity-widget.component';

describe('LeftNavActivityWidgetComponent', () => {
  let component: LeftNavActivityWidgetComponent;
  let fixture: ComponentFixture<LeftNavActivityWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftNavActivityWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftNavActivityWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
