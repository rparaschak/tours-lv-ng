import { Component, Input, OnChanges, OnInit, SimpleChange } from '@angular/core';
import { ActivityService } from '../activity.service';

@Component({
  selector: 'app-left-nav-activity-widget',
  templateUrl: './left-nav-activity-widget.component.html',
  styleUrls: ['./left-nav-activity-widget.component.scss']
})
export class LeftNavActivityWidgetComponent implements OnInit, OnChanges{

  @Input() tourActivity: any;
  public pointsPassed: number;

  constructor(private activityService: ActivityService) {
  }

  activityCheckin() {
    this.activityService.activityCheckin()
      .subscribe();
  }

  /**
   * This method fires every time is changes in activity service. Tour points counting logic.
   * @param newActivity
   */
  onActivityChange(newActivity) {
    if (!newActivity) {
      return;
    }

    this.tourActivity = newActivity;
    this.pointsPassed = this.tourActivity.checkins.length;
  }

  ngOnInit() {
  }

  ngOnChanges(changes) {
    if (changes.tourActivity && changes.tourActivity.currentValue) {
      this.onActivityChange(changes.tourActivity.currentValue);
    }
  }

}
