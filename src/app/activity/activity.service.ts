import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Tour } from '../tours/tour.interface';
import { catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ToursHttpClient } from '../core/tours-http-client';
import { LocationService } from './location.service';
import 'rxjs/add/operator/catch';
declare const M;


@Injectable()
export class ActivityService{

  private activity = new BehaviorSubject(null);

  constructor(private http: ToursHttpClient, private locationService: LocationService) {
    this.getTourActivityStatus()
      .subscribe();
  }

  getActivity(): Observable<any> {
    return this.activity;
  }

  startTour(tour: Tour): Observable<any> {
    return this.http.post<any>('/activity', {tourId: tour.id})
      .map((tourResponse) => {
        this.activity.next(tourResponse);
        return tourResponse;
      })
      .pipe(catchError(this.handleError('getTourActivityStatus', {})));
  }

  activityCheckin(): Observable<any> {
    return this.locationService.getLocation()
      .mergeMap(location => {
          return this.http.post<any>('/activity/checkin', {userLocation: location})
            .catch(errorResponse => {
              M.toast({
                html: 'Out of range.',
                displayLength: 2000
              });
              return Observable.of(errorResponse);
            });
      })
      .mergeMap(response => {
        return this.getTourActivityStatus();
      });
  }

  getTourActivityStatus(): Observable<any> {
    return this.http.get<any>('/activity')
      .map((activity) => {
        if (activity._id) {
          this.activity.next(activity);
        } else {
          this.activity.next(null);
        }
        return activity;
      })
      .pipe(catchError(this.handleError('getTourActivityStatus', {})));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error('Error', error);
      return Observable.of(result as T);
    };
  }

}
