import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ToursHttpClient{

  static API_ENDPOINT = 'http://localhost:8000';

  constructor(private httpClient: HttpClient) {
  }

  get<T>(url, options?: object): Observable<T>;

  get(url, options?: object): Observable<Object> {
    url = ToursHttpClient.API_ENDPOINT + url;
    return this.httpClient.get(url, options);
  }

  post<T>(url: string, body: any | null, options?: any): Observable<T>;

  post(url: string, body: any | null, options?: any): Observable<any> {
    url = ToursHttpClient.API_ENDPOINT + url;
    return this.httpClient.post(url, body, options);
  }
}
