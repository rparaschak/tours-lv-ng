import { Component, OnInit, AfterViewInit } from '@angular/core';

declare const M;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit{

  private navInstance;

  constructor() {
  }

  ngOnInit() {
  }

  openLeftNav() {
    this.navInstance.open();
  }

  ngAfterViewInit() {
    const leftNav = document.querySelectorAll('app-left-nav');
    this.navInstance = M.Sidenav.init(leftNav)[0];
  }


}
