import { Component, OnInit } from '@angular/core';
import { ActivityService } from '../../activity/activity.service';
import { ɵrenderComponent } from '@angular/core';
declare const M;

@Component({
  selector: 'app-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.scss']
})
export class LeftNavComponent implements OnInit{

  public user: any = {
    name: 'Momo Nunu',
    email: 'momo@gmail.com',
    profilePicture: '/assets/images/temp/left-menu-profile.jpg'
  };

  public activity: any;

  constructor(private activityService: ActivityService) {
  }

  ngOnInit() {

    /**
     * Subscribing to activity changes
     */
    this.activityService.getActivity().subscribe((activity) => {
      this.activity = activity;
    });
  }

  showSigninForm() {
    const modalElems = document.querySelector('app-modal');
    debugger;
    const instance = M.Modal.init(modalElems);
    instance.open();
  }

}
