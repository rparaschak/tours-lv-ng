import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DividerComponent } from '../core/divider/divider.component';
import { LeftNavComponent } from './left-nav/left-nav.component';
import { HeaderComponent } from './header/header.component';
import { RoutingModule } from '../routing.module';
import { ActivityModule } from '../activity/activity.module';
import { ToursModule } from '../tours/tours.module';
import { ToursHttpClient } from './tours-http-client';
import { ModalComponent } from './modal/modal.component';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ActivityModule
  ],
  declarations: [
    DividerComponent, LeftNavComponent, HeaderComponent, ModalComponent],
  providers: [
    ToursHttpClient
  ],
  exports: [
    DividerComponent, LeftNavComponent, HeaderComponent, ModalComponent
  ]
})
export class CoreModule { }
